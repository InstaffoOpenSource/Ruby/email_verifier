lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'email_verifier/version'

Gem::Specification.new do |gem|
  gem.name          = 'email_verifier'
  gem.version       = EmailVerifier::VERSION
  gem.authors       = ['Patrick Lerner', 'Kamil Ciemniewski']
  gem.email         = ['patrick@instaffo.de', 'kamil@endpoint.com']
  gem.description   = 'Helper utility checking if given email address exists or not'
  gem.summary       = 'Helper utility checking if given email address exists or not'
  gem.homepage      = 'http://endpoint.com'

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']
  gem.add_runtime_dependency('dnsruby', ['>= 1.5'])
  gem.add_runtime_dependency('proxifier')
  gem.add_development_dependency 'rspec'
  gem.license = 'MIT'
end
