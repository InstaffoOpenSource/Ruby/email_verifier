require 'spec_helper'
require 'net/smtp'

describe EmailVerifier::Checker do
  describe 'proxy connection' do
    before(:each) do
      allow_any_instance_of(described_class).to receive(:list_mxs).and_return(
        [{ address: 'mx.nikolai.info' }]
      )
    end

    context 'with proxy_url' do
      subject { described_class.new('nikolai.info', 'socks://nikolai.one:1234') }

      it 'connects via proxy' do
        expect(Net::SMTP).to_not receive(:start)
        expect(Net::ProxySMTP).to receive(:start)

        subject.connect
      end
    end

    context 'without proxy_url' do
      subject { described_class.new('nikolai.info') }

      it 'connects normally' do
        expect(Net::SMTP).to receive(:start)
        expect(Net::ProxySMTP).to_not receive(:start)
        subject.connect
      end
    end
  end
end
