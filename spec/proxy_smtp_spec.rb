require 'spec_helper'
require 'net/smtp'

describe Net::ProxySMTP do
  subject { described_class.new('nikolai.info', 21) }

  def run_tcp_socket
    subject.send(:tcp_socket, 'nikolai.info', 21)
  end

  describe 'proxy connection' do
    context 'with proxy_url' do
      it 'connects via proxy' do
        proxy_double = double
        expect(proxy_double).to receive(:open)

        subject.proxy_url = 'socks://nikolai.one:1234'
        allow(subject).to receive(:proxy) { proxy_double }
        run_tcp_socket
      end
    end

    context 'without proxy_url' do
      it 'raises exception' do
        expect { subject.send(:tcp_socket) }.to raise_error(ArgumentError)
      end
    end
  end
end
